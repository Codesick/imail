from aiohttp import web
import setproctitle
import asyncio
import platform
import logging
from prometheus_async import aio
import aiosmtplib
import aio_pika

from configuration import prepare
from utils import MailRouter, get_data_from_request
from schemas import Alert, Queues
import settings

log = logging.getLogger('imail')


async def connect_to_services(app):
    loop = app['loop']
    #
    # sa_engine = await db_connect(loop)
    # app['sa_engine'] = sa_engine
    # db_ops = DbOps(sa_engine)

    smtp = aiosmtplib.SMTP(hostname=settings.MAIL_URL, port=settings.MAIL_PORT, loop=loop)
    # app['smtp_connect_response'] = await smtp.connect()
    app['smtp_connection'] = smtp

    app['rmq_conn'] = rabbit_connection = await aio_pika.connect_robust(**settings.RABBIT_CREDENTIAL, loop=loop)
    app['rmq_channel'] = rmq_channel = await rabbit_connection.channel()

    app['mail_compose'] = MailRouter(app)


async def graceful_shutdown(app):
    await app['rmq_conn'].close()


async def hadleAlert(request):
    data, json_body = await get_data_from_request(request, data_schema=Alert)
    log.debug(data)
    channel = request.app['rmq_channel']
    routing_key = data['queue']
    if routing_key in (i.value for i in Queues.__members__.values()):
        await channel.default_exchange.publish(
            aio_pika.Message(
                body=Alert().dumps(data).data.encode()
            ),
            routing_key=routing_key
        )
        response = {'result': 'success'}
    else:
        response = {'result': 'not_success'}
    return web.json_response(data=response)


def main():
    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)

    # redis_conn = loop.run_until_complete(get_redis_conn(loop))
    app = web.Application(loop=loop)
    app['loop'] = loop
    app.on_startup.append(connect_to_services)
    app.on_shutdown.append(graceful_shutdown)

    app.router.add_post('/alert', hadleAlert)
    app.router.add_get("/metrics", aio.web.server_stats)

    host = platform.uname()[1]
    web.run_app(app, host=host, port=settings.BIND_PORT)


if __name__ == "__main__":
    prepare()
    main()
