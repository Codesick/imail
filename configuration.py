import logging.config

from settings import LOG_CFG

log = logging.getLogger(__name__)


def prepare():
    logging.config.dictConfig(LOG_CFG)
    log.debug('configured')
