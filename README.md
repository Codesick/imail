https://docs.google.com/document/d/1DWGT6-dm8KJKpxEGTTShYWsFkRe94AzrLspktKRdp-k/edit?usp=sharing

# # email_service

Сервис для сбора алертов от сторонних систем в один емейл и его отправки 

Добавление алертов осуществляется по api, либо добавлением событий в очередь 
RabbitMQ

Service located at 1-intrack-app1.uuy


## Сервисы подключенные к email_service:

    LUNA            
    ADbot_manager   


# Заказчик
- Котельников В.Ю.

## Представитель заказчика
- Котельников В.Ю.

# Ответственные
* Котельников В.Ю.

### Python 3 ![Python](https://forthebadge.com/images/badges/made-with-python.svg)