from marshmallow import Schema
from marshmallow import fields
from enum import Enum


class OrderedSchema(Schema):
    class Meta:
        ordered = True


class UnpackException(Exception):
    pass


class IncomingCmdError(Exception):
    pass


class Alert(OrderedSchema):
    time = fields.DateTime(required=True)
    queue = fields.Str(required=True)
    service = fields.Str(required=True)
    host = fields.Str(required=True)
    type = fields.Str(required=True)
    msg = fields.Str(required=True)
    data = fields.Str(required=False, default="")


class Queues(Enum):
    AGENT_IN = 'alert_msg_agent_in'
    AGENT_OUT = 'alert_msg_agent_out'
    PROVIDER = 'alert_msg_provider'
    MONITOR = 'alert_msg_monitor'
    AD_BOT = 'adbot_manager'


class Services(Enum):
    LUNA = 'LUNA'
    FRONTUL = 'FRONTUL'


class AlertType(Enum):
    TEST = 'test'
    START = 'start'
    FAIL = 'fail'


class Language(Enum):
    ENGLISH = 'eng'
    RUSSIAN = 'ru'
