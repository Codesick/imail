import os
from schemas import Language
from typing import NamedTuple


BIND_PORT = 13312
MAIL_URL = 'mail..local'
MAIL_PORT = 77
DEBUG = False
LANG = Language.ENGLISH.value  # todo: add Language.RUSSIAN.value

RABBIT_CONNECTION_STRING = 'amqp://Dominus_Ego0:dominus@rdc-rabbit-test1:'
RABBIT_TEST = {
    'login': '1',
    'password': '1',
    'host': '1-rabbit-test1',
    'virtualhost': 'luna'
}
RABBIT_PROD = {
    'login': 'luna',
    'password': '1',
    'host': '1-rabbit-lb1.',
    'virtualhost': 'luna'
}

MESSAGE_STR_TEMPLATE = {Language.ENGLISH.value: "Was caught {} event's with:\n",
                        Language.RUSSIAN.value: "Полученно событие {} от\n"
                        }

RABBIT_CREDENTIAL = RABBIT_TEST if DEBUG else RABBIT_PROD

COMMASPACE = ', '
MAIL_RECEPIENTS = COMMASPACE.join(['1@sovcombank.ru',
                                   '2@sovcombank.ru'
                                   ])

_level = "DEBUG"  # if DEBUG else "INFO"
_pkg_name = "imail"
# _log_path = os.environ.get("STATION_LOG_PATH", f"/home/log/{_pkg_name}/") # python3.6+
_log_path = os.environ.get("MAIL_LOG_PATH", "/mail_bus/logs/")


LUNA_SERVICES = [
    {
        'name': 'agent_in',
        'queue_name': 'alert_msg_agent_in',
        'bucket_size': 10,
        'alert_delay': 30,
     },
    {
        'name': 'agent_out',
        'queue_name': 'alert_msg_agent_out',
        'bucket_size': 2,
        'alert_delay': 20,
    },
]

AD_BOT_SERVICES = [
    {
        'name': 'adbot',
        'queue_name': 'adbot_manager',
        'bucket_size': 30,
        'alert_delay': 120,
    },
]

ALERT_MAP = {
    'LUNA': LUNA_SERVICES,
    'ADBOT': AD_BOT_SERVICES,
}
LOG_CFG = {
            "version": 1,
            # for deep debug
            "disable_existing_loggers": False,
            "formatters": {
                "default": {
                    "class": "logging.Formatter",
                    "format": "[%(asctime)s] {%(module)s:%(lineno)d} %(levelname)s - %(message)s",
                    "datefmt": "%H:%M:%S"},
            },
            "handlers": {
                "console": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "default",
                    "stream": "ext://sys.stdout"
                },
                "file": {
                    "level": _level,
                    "class": "logging.handlers.TimedRotatingFileHandler",
                    "formatter": "default",
                    "filename": os.path.join(_log_path, "{}_PE.log".format(_pkg_name)),
                    "when": "midnight",
                    "backupCount": 10
                },
                # "error_file": {
                #     "level": "ERROR",
                #     "class": "logging.handlers.WatchedFileHandler",
                #     "formatter": "default",
                #     "filename": os.path.join(_log_path, "error_{}\'s_PE.log".format(_pkg_name)),
                # },
            },
            "loggers": {
                "{}".format(_pkg_name): {
                    "level": "DEBUG",
                    "handlers": ["console", "file"],
                    "propagate": True
                }
            }
        }

