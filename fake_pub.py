import asyncio
from abc import abstractmethod
import asyncio
import aio_pika
from logging import getLogger
import datetime

import settings
from schemas import Queues, Services, Alert

log = getLogger(__name__)

FAKE_MSG = {
    'time': datetime.datetime.now(),
    'queue': Queues.AGENT_IN.value,
    'service': Services.LUNA.value,
    'host': 'localhost',
    'type': 'test',
    'msg': 'hello',
}


async def main(loop):
    connection = await aio_pika.connect_robust(**settings.RABBIT_TEST, loop=loop)

    async with connection:
        routing_key = "alert_msg_agent_in"

        channel = await connection.channel()
        message = Alert().dumps(FAKE_MSG)
        print(str(message.data))
        await channel.default_exchange.publish(
            aio_pika.Message(
                # body='Hello {}'.format(routing_key).encode()
                body=message.data.encode()
            ),
            routing_key=routing_key
        )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()

