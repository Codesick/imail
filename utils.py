from typing import NamedTuple
from functools import partial
import copy
import asyncio
import logging
import aiosmtplib
import aio_pika
import ujson
from email.mime.text import MIMEText

import settings
from schemas import Alert, UnpackException, IncomingCmdError, Language


# Alert_query = NamedTuple('Alert_query', [('name', str), ('queue_name', str), ('subscriber', Subscriber)])
log = logging.getLogger('imail')


class MailRouter(object):
    def __init__(self, app):
        self.pool = {}
        self.context = app
        self.loop = app['loop']
        self.channel = app['rmq_channel']
        self.smtp_connection = app['smtp_connection']
        self.subscribers_pool = []
        self.create_services_map()

    def create_services_map(self):
        for service, queryes in settings.ALERT_MAP.items():
            if not self.pool.get(service, False):
                self.pool[service] = []
            for queue_cfg in queryes:
                worker = CommonSubscriber(service=service, parent=self, **queue_cfg)
                self.pool[service].append(worker)
                self.subscribers_pool.append(
                    {queue_cfg['name']: worker.run()}
                )
                log.debug('subscribed to {}'.format(self.subscribers_pool[-1]))

    # async def add_alert_to_pool(self, alert):


class CommonSubscriber(object):
    def __init__(self, service, parent, name, queue_name, bucket_size, alert_delay):
        self.service = service
        self.name = name
        self.queue_name = queue_name
        self.parent = parent
        self.loop = parent.loop
        self.channel = self.parent.channel
        self.lock = False

        self.bucket_size = bucket_size
        self.alert_delay = alert_delay
        self.message_agent = MessageAgent(
                                parent=self,
                                smtp_connection=self.parent.context['smtp_connection'],
        )

        self.alert_cnt = 0
        self._listen_task = None
        self._timer_task = None

    def run(self):
        self._listen_task = asyncio.ensure_future(self._consume(), loop=self.loop)
        return self._listen_task

    def _msg_unpack(self, msg):
        try:
            unmarshalled = Alert().loads(msg)
            if unmarshalled.errors:
                raise UnpackException("Unpack message error: \n {}".format(unmarshalled.errors))
            unmarshaled = unmarshalled.data
        except Exception as e:
            log.exception(e)
        else:
            return unmarshaled

    async def _delayed_task(self, task):
        await asyncio.sleep(self.alert_delay)
        log.debug('start delayed task')
        await task()

    async def _on_msg(self, msg):
        msg = self._msg_unpack(msg)
        log.debug('unpacked message:\n{}'.format(msg))
        if msg:
            if self.alert_cnt == 0:
                self._timer_task = asyncio.ensure_future(
                                            self._delayed_task(self.message_agent.send_mail),
                                            loop=self.loop
                                            )
            self.alert_cnt = self.message_agent.add_alert_to_mail(msg)
            if self.alert_cnt >= self.bucket_size:
                # todo: self.lock=True and send mail
                await self.message_agent.send_mail()

    async def _consume(self):
        try:
            queue = await self.channel.declare_queue(self.queue_name)
        except Exception as e:
            log.exception(e)
        log.debug(queue)
        async for message in queue:
            # print(message)
            with message.process():
                log.info(message.body)
                await self._on_msg(message.body.decode())

                # if queue.name in message.body.decode():
                #     break

    async def send_mail(self):
        await asyncio.sleep(1)


class MessageAgent(object):
    def __init__(self, parent, smtp_connection, bucket_size=1, alert_timeout=0):
        self.loop = parent.loop
        self.parent = parent
        self.smtp_connection = None
        # self.smtp_connection = parent.parent.context['smtp_connection']
        # self.bucket_size = bucket_size
        # self.alert_timeout = alert_timeout
        # self.alert_cnt = 0
        self.alert_pool = []

    def add_alert_to_mail(self, msg):
        self.alert_pool.append(msg)
        return len(self.alert_pool)

    def _gen_mail(self, mail_body, host):
        # todo: change LuNa for parent.service
        mail = MIMEText(mail_body)
        mail['From'] = 'monitoring@sovcombank.ru'
        mail['To'] = settings.MAIL_RECEPIENTS
        mail['Subject'] = 'Мониторинг {}'.format(host)
        return mail

    def _format_and_make_mail(self, msg):
        def make_str_from_alert(alert_msg):
            return "\t- {}\n".format(alert_msg)

        message_stings = []
        data = {}
        for alert in msg:
            if alert['type'] in data:
                data[alert['type']].append(alert['msg'])
                if host != alert['host']:
                    log.warning("multy host in message query {} {}".format(host, alert['host']))
            else:
                host = alert['host']
                data[alert['type']] = [alert['msg']]
        log.debug(data)
        for event_type, events in data.items():
            message_stings.append(settings.MESSAGE_STR_TEMPLATE[settings.LANG].format(event_type))
            message_stings.extend(map(make_str_from_alert, events))
        mail_body = "".join(message_stings)
        return self._gen_mail(mail_body, host)

    async def send_mail(self):
        if self.alert_pool:
            message = copy.deepcopy(self.alert_pool)
            del self.alert_pool[:]
            self.parent.alert_cnt = 0
            # log.info('sended to email {}'.format(message))
            mail = self._format_and_make_mail(message)
            log.debug(mail)
            self.smtp_connection = aiosmtplib.SMTP(hostname=settings.MAIL_URL,
                                                   port=settings.MAIL_PORT,
                                                   loop=self.loop
                                                   )
            try:
                await self.smtp_connection.connect()
                await self.smtp_connection.send_message(mail)
            except Exception as e:
                log.exception(e)
            log.debug('DONE')


async def json_from_request(request) -> dict:
    try:
        content = await request.read()
        # log.debug(content)
        obj = ujson.loads(content.decode())
    except ValueError:
        raise IncomingCmdError("Input doesn't represent valid json structure")
    except Exception:
        log.exception("Failed to get json object")
        raise IncomingCmdError()
    return obj


async def get_data_from_request(request, data_schema=None):
    obj = await json_from_request(request)
    log.info("Got cmd: {}\n{}".format(request, obj))
    if data_schema:
        if type(obj) is str:
            unmarshalled = data_schema().loads(obj)
        elif type(obj) is dict:
            unmarshalled = data_schema().load(obj)
        else:
            UnpackException("Invalid recieved object type")
        if unmarshalled.errors:
            raise UnpackException("Unmarshalled errors: \n {}".format(unmarshalled.errors))
        unmarshaled = unmarshalled.data
    return unmarshaled, obj
